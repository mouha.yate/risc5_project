RV_CCC = ../../tools/riscv/bin/riscv-none-elf-gcc
RV_AS  = ../../tools/riscv/bin/riscv-none-elf-as
RV_LD  = ../../tools/riscv/bin/riscv-none-elf-ld
RV_OBJ = ../../tools/riscv/bin/riscv-none-elf-objdump

ASFLAGS= -march=rv32i -mabi=ilp32
GCCFLAGS=-march=rv32i -mabi=ilp32 -O2 -fno-stack-protector -w -Wl,--no-relax
LDFLAGS=-T linker.ld -m elf32lriscv -nostdlib

firmware_words=../../tools/firmware_words/firmware_words

